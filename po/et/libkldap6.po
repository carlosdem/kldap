# translation of libkldap.po to Estonian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <bald@smail.ee>, 2007-2008, 2009, 2014, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: libkldap\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-15 01:39+0000\n"
"PO-Revision-Date: 2020-06-13 19:41+0300\n"
"Last-Translator: Marek Laane <qiilaq69@gmail.com>\n"
"Language-Team: Estonian <kde-et@lists.linux.ee>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 19.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: core/ldapconnection.cpp:124
#, kde-format
msgid "No LDAP Support..."
msgstr "LDAP-i toetus puudub..."

#: core/ldapconnection.cpp:239
#, kde-format
msgid "An error occurred during the connection initialization phase."
msgstr "Ühenduse initsialiseerimise ajal tekkis viga."

#: core/ldapconnection.cpp:246
#, kde-format
msgid "Cannot set protocol version to %1."
msgstr "Protokollile versiooni %1 määramine nurjus."

#: core/ldapconnection.cpp:257
#, kde-format
msgid "Cannot set timeout to %1 second."
msgid_plural "Cannot set timeout to %1 seconds."
msgstr[0] "Aegumise määramine %1 sekundile nurjus."
msgstr[1] "Aegumise määramine %1 sekundile nurjus."

#: core/ldapconnection.cpp:269
#, kde-format
msgid "Could not set CA certificate file."
msgstr "SK sertifikaadifaili määramine nurjus."

#: core/ldapconnection.cpp:294
#, kde-format
msgid "Invalid TLS require certificate mode."
msgstr "Vigane TLS päringu sertifikaadi režiim."

#: core/ldapconnection.cpp:299
#, kde-format
msgid "Could not set TLS require certificate mode."
msgstr "TLS päringu sertifikaadi režiimi määramine nurjus."

#: core/ldapconnection.cpp:308
#, kde-format
msgid "Could not initialize new TLS context."
msgstr "Uue TLS konteksti initsialiseerimine nurjus."

#: core/ldapconnection.cpp:325
#, kde-format
msgid "TLS support not available in the LDAP client libraries."
msgstr "TLS-i toetus pole LDAP-i kliendi teekides saadaval."

#: core/ldapconnection.cpp:335
#, kde-format
msgid "Cannot set size limit."
msgstr "Suuruselimiidi määramine nurjus."

#: core/ldapconnection.cpp:345
#, kde-format
msgid "Cannot set time limit."
msgstr "Ajalimiidi määramine nurjus."

#: core/ldapconnection.cpp:353
#, kde-format
msgid "Cannot initialize the SASL client."
msgstr "SASL kliendi initsialiseerimine nurjus."

#: core/ldapconnection.cpp:430
#, kde-format
msgid ""
"LDAP support not compiled in. Please recompile libkldap with the OpenLDAP "
"(or compatible) client libraries, or complain to your distribution packagers."
msgstr ""
"LDAP-i toetus pole saadaval... Palun kompileeri libkldap OpenLDAP-i või "
"sellega ühilduva kliendi teekidega või kurda muret oma distributsiooni "
"pakendajatele."

#: core/ldapsearch.cpp:222
#, kde-format
msgid "Cannot access to server. Please reconfigure it."
msgstr "Ligipääs serverile puudub. Palun muuda seadistusi."

#: widgets/addhostdialog.cpp:67
#, kde-format
msgctxt "@title:window"
msgid "Add Host"
msgstr "Masina lisamine"

#: widgets/ldapconfigurewidget.cpp:95
#, kde-format
msgctxt "@title:window"
msgid "Edit Host"
msgstr "Masina muutmine"

#: widgets/ldapconfigurewidget.cpp:112
#, kde-format
msgid "Do you want to remove setting for host \"%1\"?"
msgstr "Kas eemaldada masina \"%1\" seadistus?"

#: widgets/ldapconfigurewidget.cpp:113
#, kde-format
msgid "Remove Host"
msgstr "Masina eemaldamine"

#: widgets/ldapconfigurewidget.cpp:270
#, kde-format
msgid "Check all servers that should be used:"
msgstr "Kõigi kasutatavate serverite kontroll:"

#: widgets/ldapconfigurewidget.cpp:304
#, kde-format
msgid "&Add Host..."
msgstr "&Lisa masin ..."

#: widgets/ldapconfigurewidget.cpp:306
#, kde-format
msgid "&Edit Host..."
msgstr "&Muuda masinat ..."

#: widgets/ldapconfigurewidget.cpp:309
#, kde-format
msgid "&Remove Host"
msgstr "&Eemalda masin"

#: widgets/ldapconfigwidget.cpp:90
#, kde-format
msgid "User:"
msgstr "Kasutaja:"

#: widgets/ldapconfigwidget.cpp:97
#, kde-format
msgid "Bind DN:"
msgstr "Bind DN:"

#: widgets/ldapconfigwidget.cpp:104
#, kde-format
msgid "Realm:"
msgstr "Tsoon:"

#: widgets/ldapconfigwidget.cpp:112
#, kde-format
msgid "Password:"
msgstr "Parool:"

#: widgets/ldapconfigwidget.cpp:119
#, kde-format
msgid "Host:"
msgstr "Masin:"

#: widgets/ldapconfigwidget.cpp:128
#, kde-format
msgid "Port:"
msgstr "Port:"

#: widgets/ldapconfigwidget.cpp:136
#, kde-format
msgid "LDAP version:"
msgstr "LDAP versioon:"

#: widgets/ldapconfigwidget.cpp:144
#, kde-format
msgctxt "default ldap size limit"
msgid "Default"
msgstr "Vaikimisi"

#: widgets/ldapconfigwidget.cpp:145
#, kde-format
msgid "Size limit:"
msgstr "Suuruselimiit:"

#: widgets/ldapconfigwidget.cpp:153
#, kde-format
msgid " sec"
msgstr " sek"

#: widgets/ldapconfigwidget.cpp:154
#, kde-format
msgctxt "default ldap time limit"
msgid "Default"
msgstr "Vaikimisi"

#: widgets/ldapconfigwidget.cpp:155
#, kde-format
msgid "Time limit:"
msgstr "Ajalimiit:"

#: widgets/ldapconfigwidget.cpp:163
#, kde-format
msgid "No paging"
msgstr "Ei saalita"

#: widgets/ldapconfigwidget.cpp:164
#, kde-format
msgid "Page size:"
msgstr "Saale suurus:"

#: widgets/ldapconfigwidget.cpp:175 widgets/ldapconfigwidget.cpp:253
#, kde-format
msgid "Query Server"
msgstr "Serveri päring"

#: widgets/ldapconfigwidget.cpp:185
#, kde-format
msgctxt "Distinguished Name"
msgid "DN:"
msgstr "DN:"

#: widgets/ldapconfigwidget.cpp:192
#, kde-format
msgid "Filter:"
msgstr "Filter:"

#: widgets/ldapconfigwidget.cpp:201
#, kde-format
msgctxt "@option:radio set no security"
msgid "No"
msgstr "Puudub"

#: widgets/ldapconfigwidget.cpp:204
#, kde-format
msgctxt "@option:radio use TLS security"
msgid "TLS"
msgstr "TLS"

#: widgets/ldapconfigwidget.cpp:207
#, kde-format
msgctxt "@option:radio use SSL security"
msgid "SSL"
msgstr "SSL"

#: widgets/ldapconfigwidget.cpp:222
#, fuzzy, kde-format
#| msgid "Security"
msgid "Security:"
msgstr "Turvalisus"

#: widgets/ldapconfigwidget.cpp:232
#, kde-format
msgctxt "@option:radio anonymous authentication"
msgid "Anonymous"
msgstr "Anonüümne"

#: widgets/ldapconfigwidget.cpp:235
#, kde-format
msgctxt "@option:radio simple authentication"
msgid "Simple"
msgstr "Lihtne"

#: widgets/ldapconfigwidget.cpp:238
#, kde-format
msgctxt "@option:radio SASL authentication"
msgid "SASL"
msgstr "SASL"

#: widgets/ldapconfigwidget.cpp:241
#, fuzzy, kde-format
#| msgid "Authentication"
msgid "Authentication:"
msgstr "Autentimine"

#: widgets/ldapconfigwidget.cpp:259
#, kde-format
msgid "SASL mechanism:"
msgstr "SASL mehhanism:"

#: widgets/ldapconfigwidget.cpp:303
#, kde-format
msgid "Check server"
msgstr "Serveri kontroll"

#: widgets/ldapconfigwidget.cpp:309
#, kde-format
msgctxt "@title:window"
msgid "LDAP Query"
msgstr "LDAP-i päring"

#: widgets/ldapconfigwidget.cpp:323
#, kde-format
msgctxt "%1 is a url to ldap server"
msgid "Unknown error connecting %1"
msgstr "Tundmatu tõrge ühendumisel serveriga %1"

#~ msgid ""
#~ "LDAP password is stored as clear text, do you want to store it in kwallet?"
#~ msgstr ""
#~ "LDAP-i parool on salvestatud lihttekstina, kas soovid salvestada selle "
#~ "KDE turvalaekasse?"

#, fuzzy
#~| msgid "Store clear text password in KWallet"
#~ msgid "Store clear text password in Wallet"
#~ msgstr "Lihttekstis parooli salvestamine KDE turvalaekasse"

#~ msgid "LDAP Servers"
#~ msgstr "LDAP-serverid"

#~ msgid "Attribute"
#~ msgstr "Atribuut"

#~ msgid "Value"
#~ msgstr "Väärtus"

#~ msgid "Distinguished Name"
#~ msgstr "Eraldusnimi"

#~ msgid ""
#~ "SASL support is not available. Please recompile libkldap with the Cyrus-"
#~ "SASL (or compatible) client libraries, or complain to your distribution "
#~ "packagers."
#~ msgstr ""
#~ "SASL-i toetus pole saadaval. Palun kompileeri libkldap Cyrus-SASL-i või "
#~ "sellega ühilduva kliendi teekidega või kurda muret oma distributsiooni "
#~ "pakendajatele."

#~ msgid "LDAP Operations error"
#~ msgstr "LDAP-i toimingute viga"

#, fuzzy
#~| msgid "LDAP version:"
#~ msgid "LDAP Server Settings"
#~ msgstr "LDAP versioon:"

#, fuzzy
#~| msgid "User:"
#~ msgid "User ID"
#~ msgstr "Kasutaja:"
